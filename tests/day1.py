import unittest
import numpy as np

from days import day1

test_data = [
    199,
    200,
    208,
    210,
    200,
    207,
    240,
    269,
    260,
    263
]

class MyTestCase(unittest.TestCase):
    def test_increasing_count(self):
        self.assertEqual(day1.increasing_count(test_data), 7)
    def test_increasing_count_integration(self):
        real_test_data = np.loadtxt("data_day1.txt")
        print(day1.increasing_count(real_test_data))
        self.assertEqual(day1.increasing_count(real_test_data),1715)

    def test_increasing_count_sliding_window(self):
        self.assertEqual(day1.increasing_count_sliding_window(test_data), 5)
    def test_increasing_count_sliding_window_integration(self):
        real_test_data = np.loadtxt("data_day1.txt")
        print(day1.increasing_count_sliding_window(real_test_data))
        self.assertEqual(day1.increasing_count_sliding_window(real_test_data), 1739)


    def test_increasing_count_sliding_window_alt(self):
        self.assertEqual(day1.increasing_count_sliding_window_alt(test_data), 5)
    def test_increasing_count_sliding_window_integration_alt(self):
        real_test_data = np.loadtxt("data_day1.txt")
        print(day1.increasing_count_sliding_window_alt(real_test_data))
        self.assertEqual(day1.increasing_count_sliding_window_alt(real_test_data), 1739)



if __name__ == '__main__':
    unittest.main()
