
def increasing_count(data):

    count = 0
    for i in range(1, len(data)):
        if data[i] > data[i - 1]:
            count += 1
    return count


def increasing_count_sliding_window(data):
    '''Fixed size sliding window'''
    count = 0
    window_sum = []
    for window_start in range(len(data)-2):
        single_letter_sum = data[window_start+0] + data[window_start+1] + data[window_start+2]
        window_sum.append(single_letter_sum)
        if window_sum[window_start - 1] < window_sum[window_start]:
            count += 1
    return count


def increasing_count_sliding_window_alt(data):
    '''Fixed size sliding window'''
    count = 0
    for i in range(len(data) - 3):
        if data[i] < data[i+3]:
            count += 1
    return count
